//Simlation of VANET using NS2
Requirements:
NS-allinone-2.35
Ubuntu 16.04
SUMO_0.29.0
Python
Download a map of any city of our choice from openstreetmap.org

Installation:
Install all the required tools.
After the installation, open terminal and enter ns.
You should be prompted to - %.
Enter exit 

Simulation:
For the simulation we have to create a bunch of files using the commands provided in sourcecode.txt file
Analysis:
After the simulation, use ns2 trace analyser tool to analyse the trace file.
upload the tcl file and tr file in the tool to calculate delay, jitter, threshold.
